package ru.codeuniverse.apicomparer;

import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {

	public static Stage primaryStage;

	@Override
	public void start(Stage primaryStage) {
		try {
			primaryStage = primaryStage;
			AnchorPane root = (AnchorPane) this.getRoot();
			System.out.println("root is: " + root);
			Scene scene = new Scene(root);
			scene.getStylesheets().add(
					getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	Parent getRoot() {
		String fxmlResourceName = "MainForm.fxml";
		URL fxmlResource = getClass().getResource(fxmlResourceName);
		FXMLLoader loader = new FXMLLoader(fxmlResource);
		try {
			Parent root = loader.load();
			return root;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null; // if FXML loader failed to load main form
	}

	public static void main(String[] args) {
		launch(args);
	}
}
