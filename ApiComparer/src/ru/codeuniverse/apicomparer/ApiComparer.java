package ru.codeuniverse.apicomparer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ApiComparer {

	// returns method names encountered only in file 1
	// (out of two chosen files)
	public Set<String> getFile1UniqueNames(File file1, File file2)
			throws IOException {

		// return empty Set if either arg is null
		if ((file1 == null) && (file2 == null)) {
			return new TreeSet<>();
		}

		List<String> linesFromFile1 = new ArrayList<>();
		linesFromFile1 = getLinesFromFile(file1);
		TreeSet<String> methodNames1 = getMethodNames(linesFromFile1);

		List<String> linesFromFile2 = new ArrayList<>();
		linesFromFile2 = getLinesFromFile(file2);
		TreeSet<String> methodNames2 = getMethodNames(linesFromFile2);

		TreeSet<String> uniqueNames = new TreeSet<>();
		for (String methodName : methodNames1) {
			if (!methodNames2.contains(methodName)) {
				uniqueNames.add(methodName);
			}
		}
		return uniqueNames;
	}

	// returns method names encountered only in file 2
	// (out of two chosen files)
	public Set<String> getFile2UniqueNames(File file1, File file2)
			throws IOException {

		// return empty Set if either arg is null
		if ((file1 == null) && (file2 == null)) {
			return new TreeSet<>();
		}

		List<String> linesFromFile1 = new ArrayList<>();
		linesFromFile1 = getLinesFromFile(file1);
		TreeSet<String> methodNames1 = getMethodNames(linesFromFile1);

		List<String> linesFromFile2 = new ArrayList<>();
		linesFromFile2 = getLinesFromFile(file2);
		TreeSet<String> methodNames2 = getMethodNames(linesFromFile2);

		TreeSet<String> uniqueNames = new TreeSet<>();
		for (String methodName : methodNames2) {
			if (!methodNames1.contains(methodName)) {
				uniqueNames.add(methodName);
			}
		}
		return uniqueNames;
	}

	public Set<String> getDuplicateNames(File file1, File file2)
			throws IOException {

		// return empty Set if either arg is null
		if ((file1 == null) && (file2 == null)) {
			return new TreeSet<>();
		}

		List<String> linesFromFile1 = new ArrayList<>();
		linesFromFile1 = getLinesFromFile(file1);
		TreeSet<String> methodNames1 = getMethodNames(linesFromFile1);

		List<String> linesFromFile2 = new ArrayList<>();
		linesFromFile2 = getLinesFromFile(file2);
		TreeSet<String> methodNames2 = getMethodNames(linesFromFile2);

		TreeSet<String> duplicateNames = new TreeSet<>();
		for (String methodName : methodNames1) {
			if (methodNames2.contains(methodName)) {
				duplicateNames.add(methodName);
			}
		}
		return duplicateNames;
	}

	// returns all lines in the file
	public List<String> getLinesFromFile(File file) throws IOException {
		FileReader fileReader = new FileReader(file);
		BufferedReader br = new BufferedReader(fileReader);
		List<String> result = new ArrayList<>();
		String line;
		while ((line = br.readLine()) != null) {
			result.add(line);
		}
		br.close();
		return result;
	}

	// extracts unique method Names from its arg String
	public TreeSet<String> getMethodNames(List<String> src) {
		TreeSet<String> result = new TreeSet<>();

		String methodName = null;
		String[] tmp;
		for (String string : src) {
			tmp = string.split("\\(");
			if (tmp != null) {
				methodName = tmp[0].trim();
			}
			result.add(methodName);
		}
		System.out.println(result);
		return result;
	}

	// public static void main(String[] args) {
	// String s = "hello";
	// String regexp = Pattern.quote("(");
	// String[] arr = s.split(regexp);
	// System.out.println(Arrays.toString(arr));
	// }

}
