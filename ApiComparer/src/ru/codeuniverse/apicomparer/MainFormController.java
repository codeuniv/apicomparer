package ru.codeuniverse.apicomparer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Set;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class MainFormController {

	File firstFile;
	File secondFile;

	@FXML
	private TextArea duplicatsTextArea;

	@FXML
	private ScrollPane resultScrollPane;

	@FXML
	private Button btnFirst;

	@FXML
	private Button btnSecond;

	@FXML
	private Button duplicatesBtn;

	@FXML
	private Button f1_UniqBtn;

	@FXML
	private Button f2_UniqBtn;

	@FXML
	private Button copyBtn;

	@FXML
	void onBtnFirstAction(ActionEvent event) {

		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open First file");
		fileChooser.getExtensionFilters()
				.addAll(new ExtensionFilter("Text Files", "*.txt"));
		firstFile = fileChooser.showOpenDialog(Main.primaryStage);
		// if user closed open file dialog w/o choosing any file - do nothing
		if (secondFile == null) {
			return;
		}
		// change tooltip for Choose-First-File-Button
		String tooltipText = firstFile.toString();
		Tooltip firstBtnTooltip = new Tooltip(tooltipText);
		btnFirst.setTooltip(firstBtnTooltip);
	}

	@FXML
	void onBtnSecondAction(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open First file");
		fileChooser.getExtensionFilters()
				.addAll(new ExtensionFilter("Text Files", "*.txt"));
		secondFile = fileChooser.showOpenDialog(Main.primaryStage);
		// if user closed open file dialog w/o choosing any file - do nothing
		if (secondFile == null) {
			return;
		}
		// set tooltip for Choose-Second-File-Button
		String tooltipText = secondFile.toString();
		Tooltip secondBtnTooltip = new Tooltip(tooltipText);
		btnSecond.setTooltip(secondBtnTooltip);
	}

	@FXML
	void onBtnDuplicatesAction(ActionEvent event) {
		ApiComparer apiComparer = new ApiComparer();
		try {
			Set<String> duplicateNames = apiComparer
					.getDuplicateNames(firstFile, secondFile);
			String tmp = duplicateNames.toString();
			tmp = tmp.replace('[', ' ');
			tmp = tmp.replace(']', ' ');
			String duplicateNamesFormatted = tmp.replace(",",
					System.lineSeparator());
			duplicatsTextArea.setWrapText(true);
			duplicatsTextArea.setText(duplicateNamesFormatted);

		} catch (FileNotFoundException e) {
			// TODO
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void on_F1_UniqBtn_Action(ActionEvent event) {
		ApiComparer apiComparer = new ApiComparer();
		try {
			Set<String> duplicateNames = apiComparer
					.getFile1UniqueNames(firstFile, secondFile);
			String tmp = duplicateNames.toString();
			tmp = tmp.replace('[', ' ');
			tmp = tmp.replace(']', ' ');
			String duplicateNamesFormatted = tmp.replace(",",
					System.lineSeparator());
			duplicatsTextArea.setWrapText(true);
			duplicatsTextArea.setText(duplicateNamesFormatted);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void on_F2_UniqBtn_Action(ActionEvent event) {
		ApiComparer apiComparer = new ApiComparer();
		try {
			Set<String> duplicateNames = apiComparer
					.getFile2UniqueNames(firstFile, secondFile);
			String tmp = duplicateNames.toString();
			tmp = tmp.replace('[', ' ');
			tmp = tmp.replace(']', ' ');
			String duplicateNamesFormatted = tmp.replace(",",
					System.lineSeparator());
			duplicatsTextArea.setWrapText(true);
			duplicatsTextArea.setText(duplicateNamesFormatted);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	private void initialize() {
		// set tooltip for Find-Duplicates-Button
		String tooltipText = "Methods with same name";
		Tooltip duplicatesBtnTooltip = new Tooltip(tooltipText);
		duplicatesBtn.setTooltip(duplicatesBtnTooltip);

		// set tooltip for Choose-First-File-Button
		tooltipText = "Choose first file";
		Tooltip firstBtnTooltip = new Tooltip(tooltipText);
		btnFirst.setTooltip(firstBtnTooltip);

		// set tooltip for Choose-Second-File-Button
		tooltipText = "Choose second file";
		Tooltip secondBtnTooltip = new Tooltip(tooltipText);
		btnSecond.setTooltip(secondBtnTooltip);

		// set copy button icon
		String imgSource = "/clipboard.png";
		Image img = new Image(imgSource, 40, 0, true, true);
		ImageView iv = new ImageView(img);
		copyBtn.setGraphic(iv);
	}

	@FXML
	void onCopyBtnAction(ActionEvent event) {
		Clipboard clipboard = Clipboard.getSystemClipboard();
		ClipboardContent clipboardContent = new ClipboardContent();
		clipboardContent.putString(duplicatsTextArea.getText());
		clipboard.setContent(clipboardContent);
	}

}
